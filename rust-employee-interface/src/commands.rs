use std::collections::HashMap;
pub enum Command {
    Add {
        employee: String,
        department: String,
    },
    List(String),
    Sort,
}

impl Command {
    pub fn execute(&self, ctx: &mut HashMap<String, Vec<String>>) {
        match self {
            Command::Add {
                employee,
                department,
            } => add_employee_to_department(employee, department, ctx),
            Command::List(department) => {
                list_employees_from_department(department, ctx);
            }
            Command::Sort => {
                sort_employees(ctx);
            }
        }
    }
}

fn add_employee_to_department(
    employee: &String,
    department: &String,
    ctx: &mut HashMap<String, Vec<String>>,
) {
    match ctx.get_mut(department) {
        Some(employees) => {
            employees.push(String::from(employee));
        }
        None => {
            let employees = vec![String::from(employee)];
            ctx.insert(String::from(department), employees);
        }
    }

    println!("Employee {} added to {}", employee, department);
}

fn list_employees_from_department(department: &String, ctx: &mut HashMap<String, Vec<String>>) {
    match ctx.get(department) {
        Some(employees) => {
            println!("Employees in {}: {:?}", department, employees);
        }
        None => {
            println!("No employees found for {}", department);
        }
    }
}

fn sort_employees(ctx: &mut HashMap<String, Vec<String>>) {
    let mut all_employees = Vec::new();
    for employees in ctx.values() {
        all_employees.extend_from_slice(employees);
    }

    all_employees.sort();
    println!("All employees sorted: {:?}", all_employees);
}

pub fn parse_command(command: &str) -> Option<Command> {
    match parse_add_command(command) {
        Some(command) => {
            return Some(command);
        }
        None => {}
    }

    match parse_list_command(command) {
        Some(command) => {
            return Some(command);
        }
        None => {}
    }

    match parse_sort_command(command) {
        Some(command) => {
            return Some(command);
        }
        None => {
            return None;
        }
    }
}

fn parse_add_command(command: &str) -> Option<Command> {
    let words: Vec<&str> = command.split(" ").collect();
    if words.len() != 4 || words[0] != "Add" || words[2] != "to" {
        return None;
    }

    Some(Command::Add {
        employee: String::from(words[1]),
        department: String::from(words[3]),
    })
}

fn parse_list_command(command: &str) -> Option<Command> {
    let words: Vec<&str> = command.split(" ").collect();
    if words.len() != 2 || words[0] != "List" {
        return None;
    }

    Some(Command::List(String::from(words[1])))
}

fn parse_sort_command(command: &str) -> Option<Command> {
    let words: Vec<&str> = command.split(" ").collect();
    if words.len() != 1 || words[0] != "List" {
        return None;
    }

    Some(Command::Sort)
}
