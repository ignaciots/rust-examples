use std::collections::HashMap;
use std::io;

mod commands;

fn main() {
    println!("--- Employee manager interface ---");
    println!("");
    println!("Adding an employee to a department: 'Add <employee> to <department>'");
    println!("Listing employees from department: 'List <department'");
    println!("Listing all employees, sorted: 'List'");

    let mut ctx: HashMap<String, Vec<String>> = HashMap::new();
    loop {
        let mut input = String::new();
        println!("> ");
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line. Terminating.");
        let input = input.trim();
        match commands::parse_command(&input) {
            Some(command) => {
                command.execute(&mut ctx);
            }
            None => {
                println!("Command {} is not valid.", input);
            }
        }
    }
}
