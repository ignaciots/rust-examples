use std::io;

fn main() {
    println!("nth Fibonacci number calculator.");
    let n = ask_input("n:", "n must be 0 or positive.");
    let result = calc_fibonacci(n);
    println!("{}th fibonacci number: {}", n, result);
}

fn ask_input(input_message: &str, error_message: &str) -> u32 {
    println!("{}", input_message);
    let input: u32;
    loop {
        let mut input_str = String::new();
        io::stdin()
            .read_line(&mut input_str)
            .expect("Could not read line.");
        match input_str.trim().parse() {
            Result::Ok(num) => {
                input = num;
                break;
            }
            Result::Err(_) => {
                println!("{}", error_message);
            }
        }
    }

    input
}

fn calc_fibonacci(n: u32) -> u128 {
    if n == 0 || n == 1 {
        return n.into();
    }

    calc_fibonacci(n - 1) + calc_fibonacci(n - 2)
}
