use core::str::FromStr;
use std::io;

fn main() {
    println!("🌡 Fahrenheit to Celsius calculator 🌡");
    println!("(1) Fahrenheit -> Celsius");
    println!("(2) Celsius -> Fahrenheit");
    println!("(3) Exit");

    let mut exit_option = false;
    while !exit_option {
        match ask_input(
            "Input an option:",
            "Invalid option. Option must be a number.",
        ) {
            1 => {
                fahrenheit_to_celsius();
                exit_option = true;
            }
            2 => {
                celsius_to_fahrenheit();
                exit_option = true;
            }
            3 => exit_option = true,
            _ => {
                println!("Invalid option. Please enter a valid option.");
                exit_option = false;
            }
        }
    }
}

fn fahrenheit_to_celsius() {
    let fahrenheit: f64 = ask_input(
        "Input Fahrenheit degrees:",
        "Invalid input. Fahrenheit degrees expected",
    );
    let celsius = (fahrenheit + 40.0) / 1.8 - 40.0;
    println!("{} ºF -> {} ºC 🌡", fahrenheit, celsius);
}

fn celsius_to_fahrenheit() {
    let celsius: f64 = ask_input(
        "Input Celsius degrees:",
        "Invalid input. Celsius degrees expected",
    );
    let fahrenheit = (celsius + 40.0) * 1.8 - 40.0;
    println!("{} ºC -> {} ºF 🌡", celsius, fahrenheit);
}

fn ask_input<T: FromStr>(input_message: &str, error_message: &str) -> T {
    println!("{}", input_message);
    let input: T;
    loop {
        let mut input_str = String::new();
        io::stdin()
            .read_line(&mut input_str)
            .expect("Could not read line.");
        match input_str.trim().parse() {
            Result::Ok(num) => {
                input = num;
                break;
            }
            Result::Err(_) => {
                println!("{}", error_message);
            }
        }
    }

    input
}
