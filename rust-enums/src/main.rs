#[derive(Debug)]
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

impl Message {
    fn call(&self) {
        println!("{:?}", self);
    }
}

fn main() {
    let quit = Message::Write(String::from("hello"));
    let move_message = Message::Move { x: 1, y: 2 };

    let change_color = Message::ChangeColor(1, 2, 3);

    quit.call();
    move_message.call();
    change_color.call();

    let some_u8_value = Some(0);
    if let Some(3) = some_u8_value {
        println!("Three!");
    }

    let some_u8_value = Some(3);
    if let Some(3) = some_u8_value {
        println!("Three!");
    }
}
