const CONSONANTS: &str = "BCDFGHJKLMNPQRSTVWXYZ";
const VOWELS: &str = "AEIOU";

fn main() {
    let input = "This initial string will be pigified";

    let words: Vec<&str> = input.split_whitespace().collect();
    let mut pig_words: Vec<String> = Vec::new();
    for word in words {
        if starts_with_consonant(word) {
            let pigified_consonant = pigify_consonant(word);
            pig_words.push(pigified_consonant);
        } else if starts_with_vowel(word) {
            pig_words.push(pigify_vowel(word));
        } else {
            pig_words.push(String::from(word));
        }
    }

    println!("Original: {}", input);
    println!("Pigified: {}", pig_words.join("  "));
}

fn starts_with_consonant(word: &str) -> bool {
    for consonant in CONSONANTS.chars() {
        if word.starts_with(consonant) || word.starts_with(consonant.to_ascii_lowercase()) {
            return true;
        }
    }

    false
}

fn pigify_consonant(word: &str) -> String {
    let word = String::from(word);
    return String::from(&word[1..]) + "-" + &word[0..1].to_lowercase() + "ay";
}

fn starts_with_vowel(word: &str) -> bool {
    for vowel in VOWELS.chars() {
        if word.starts_with(vowel) || word.starts_with(vowel.to_ascii_lowercase()) {
            return true;
        }
    }

    false
}

fn pigify_vowel(word: &str) -> String {
    let word = String::from(word);
    return word + "-hay";
}
