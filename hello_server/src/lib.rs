//! Crate that contains the ThreadPool struct representation and its functions, used to initialize thread pools and execute functions on its threads.

use std::error::Error;
use std::fmt;
use std::fmt::Display;
use std::sync::mpsc;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;

/// Struct that represents a thread pool with a fixed size and the ability to execute closures on those threads.
///
pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: mpsc::Sender<Message>,
}

/// Error returned when the [ThreadPool](ThreadPool) cannot be created.
///
/// This error is returned when an invalid pool size is specified.
#[derive(Debug)]
pub struct PoolCreationError(String);

impl Display for PoolCreationError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Error: ({})", self.0)
    }
}

impl Error for PoolCreationError {}

impl ThreadPool {
    /// Create a new ThreadPool.
    ///
    /// The size is the number of threads in the pool.
    ///
    /// # Errors
    ///
    /// [PoolCreationError](PoolCreationError) will be returned if the pool size is zero.
    ///
    /// # Examples
    /// ```
    /// use hello_server::*;
    ///
    /// let my_pool = ThreadPool::new(5).unwrap();
    /// assert!(matches!(my_pool, ThreadPool))
    /// ```
    pub fn new(size: usize) -> Result<ThreadPool, PoolCreationError> {
        if size == 0 {
            return Err(PoolCreationError(format!(
                "Size of thread pool must be greater than zero. Size was: {}",
                size
            )));
        }

        let (sender, receiver) = mpsc::channel();

        let receiver = Arc::new(Mutex::new(receiver));

        let mut workers = Vec::with_capacity(size);

        for worker_id in 0..size {
            workers.push(Worker::new(worker_id, Arc::clone(&receiver)));
        }

        Ok(ThreadPool { workers, sender })
    }

    /// Execute a function on a thread of the pool.
    ///
    /// The given function will be executed by one of the threads of the pool, when available.
    ///
    /// # Examples
    /// ```
    /// use hello_server::*;
    /// use std::sync::Arc;
    /// use std::sync::Mutex;
    ///
    /// let pool = ThreadPool::new(10).unwrap();
    /// let counter = Arc::new(Mutex::new(0));
    /// for _ in 0..5 {
    ///     let counter = counter.clone();
    ///     pool.execute(move || {
    ///         let counter = counter.lock().unwrap();
    ///         let mut counter_ref = counter;
    ///         *counter_ref += 3;
    ///         println!("{}", counter_ref)
    ///     })
    /// }
    ///
    /// drop(pool);
    /// assert_eq!(*counter.clone().lock().unwrap(), 15);
    /// ```
    pub fn execute<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        let job = Box::new(f);

        self.sender.send(Message::NewJob(job)).unwrap();
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        println!("Sending terminate message to all workers.");

        for _ in &self.workers {
            self.sender.send(Message::Terminate).unwrap();
        }

        println!("Shutting down all workers.");

        for worker in &mut self.workers {
            println!("Shutting down worker {}", worker.id);

            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}

type Job = Box<dyn FnOnce() + Send + 'static>;

enum Message {
    NewJob(Job),
    Terminate,
}

struct Worker {
    id: usize,
    thread: Option<thread::JoinHandle<()>>,
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Message>>>) -> Worker {
        let thread = thread::spawn(move || loop {
            let message = receiver.lock().unwrap().recv().unwrap();

            match message {
                Message::NewJob(job) => {
                    println!("Worker {} got a job; executing.", id);

                    job();
                }
                Message::Terminate => {
                    println!("Worker {} was told to terminate.", id);

                    break;
                }
            }
        });

        Worker {
            id,
            thread: Some(thread),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_thread_pool_creation() {
        let invalid_threadpool = ThreadPool::new(0);
        assert!(matches!(invalid_threadpool, Err(_)));

        let valid_threadpool = ThreadPool::new(5);
        assert!(matches!(valid_threadpool, Ok(_)))
    }

    #[test]
    fn test_thread_pool_execution() {
        let pool = ThreadPool::new(10).unwrap();
        let counter = Arc::new(Mutex::new(0));
        for _ in 0..5 {
            let counter = counter.clone();
            pool.execute(move || {
                let counter = counter.lock().unwrap();
                let mut counter_ref = counter;
                *counter_ref += 3;
                println!("{}", counter_ref)
            })
        }

        drop(pool);
        assert_eq!(*counter.clone().lock().unwrap(), 15);
    }
}
