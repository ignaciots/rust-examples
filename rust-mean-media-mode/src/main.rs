use rand::Rng;

mod operations;

fn main() {
    let mut numbers: Vec<u32> = Vec::new();
    for _i in 0..10 {
        let gen_number = rand::thread_rng().gen_range(0..10);
        numbers.push(gen_number);
    }

    println!("Numbers: {:?}", numbers);
    let mean = operations::mean(&numbers);
    println!("Mean is: {}", mean);
    let median = operations::median(&numbers);
    println!("Median is: {}", median);
    let mode = operations::mode(&numbers);
    println!("Mode is: {}", mode);
}
