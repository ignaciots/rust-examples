use std::collections::HashMap;

pub fn mean(numbers: &[u32]) -> u32 {
    let mut sum = 0;
    let mut count = 0;
    for number in numbers {
        sum += number;
        count += 1;
    }
    sum / count
}

pub fn median(numbers: &[u32]) -> u32 {
    let mut sorted_vec = Vec::with_capacity(numbers.len());
    for number in numbers {
        sorted_vec.push(number);
    }

    let mut n = sorted_vec.len();
    loop {
        let mut newn = 0;
        for i in 1..(n) {
            if sorted_vec[i - 1] > sorted_vec[i] {
                sorted_vec.swap(i - 1, i);
                newn = i;
            }
        }
        n = newn;
        if n <= 1 {
            break;
        }
    }

    *sorted_vec[sorted_vec.len() / 2]
}

pub fn mode(numbers: &[u32]) -> u32 {
    let mut number_count = HashMap::with_capacity(numbers.len());
    for i in numbers {
        let count = number_count.entry(i).or_insert(1);
        *count += 1;
    }

    let mut mode_count = number_count[&numbers[0]];
    let mut mode = numbers[0];
    for (key, val) in number_count.iter() {
        if *val > mode_count {
            mode = **key;
            mode_count = *val;
        }
    }

    mode
}
