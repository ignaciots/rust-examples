fn main() {
    const DAYS_OF_CHRISTMAS: [&str; 12] = [
        "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eight", "ninth",
        "tenth", "eleventh", "twelfth",
    ];

    const ITEMS_OF_CHRISTMAS: [&str; 12] = [
        "A partridge in a pear tree!",
        "Two turtle doves",
        "Three french hens",
        "Four calling birds",
        "Five golden rings",
        "Six geese a layin'",
        "Seven swans a swimmin'",
        "Eight maids milkin'",
        "Nine ladies dancin'",
        "Ten lords a leapin'",
        "Eleven pipers pipin'",
        "Twelve drummers drummin'",
    ];

    for i in 0..DAYS_OF_CHRISTMAS.len() {
        if i > 0 {
            println!("{}", ITEMS_OF_CHRISTMAS[i]);
        }
        println!("On the {} day of Christmas", DAYS_OF_CHRISTMAS[i]);
        println!("My true love sent me");

        let mut items_index = i;
        while items_index != 0 {
            println!("{}", ITEMS_OF_CHRISTMAS[items_index]);
            items_index -= 1;
        }
        if i == 0 {
            println!("{}", ITEMS_OF_CHRISTMAS[0]);
        } else {
            println!("And a patridge in a pear tree!");
        }

        if i < DAYS_OF_CHRISTMAS.len() - 1 {
            println!();
        }
    }
}
